package com.example.home_work_6.converter;

import com.example.home_work_6.modal.Card;

public final class CardConverter {

    public static Card cardConverter(Card updatePerson, Card savedPerson) {
        savedPerson.setSum(updatePerson.getSum());
        savedPerson.setPerson(updatePerson.getPerson());
        savedPerson.setProductList(updatePerson.getProductList());

        return savedPerson;
    }
}
