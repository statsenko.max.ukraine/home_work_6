package com.example.home_work_6.converter;

import com.example.home_work_6.modal.Shop;

public final class ShopConverter {

    public static Shop shopConverter(Shop updateShop, Shop savedShop) {
        savedShop.setName(updateShop.getName());

        return savedShop;
    }
}
