package com.example.home_work_6.converter;

import com.example.home_work_6.modal.Person;

public final class PersonConverter {

    public static Person personConverter(Person updatePerson, Person savedPerson) {
        savedPerson.setFirstName(updatePerson.getFirstName());
        savedPerson.setLastName(updatePerson.getLastName());
        savedPerson.setAge(updatePerson.getAge());
        savedPerson.setPhoneNumber(updatePerson.getPhoneNumber());
        savedPerson.setEmail(updatePerson.getEmail());

        return savedPerson;
    }
}
