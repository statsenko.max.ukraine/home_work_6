package com.example.home_work_6.converter;

import com.example.home_work_6.modal.Product;

public final class ProductConverter {

    public static Product productConverter(Product updatePerson, Product savedPerson) {
        savedPerson.setName(updatePerson.getName());
        savedPerson.setPrice(updatePerson.getPrice());
        savedPerson.setCard(updatePerson.getCard());

        return savedPerson;
    }
}
