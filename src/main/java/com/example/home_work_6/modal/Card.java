package com.example.home_work_6.modal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "card")
public class Card {

    public Card(Double sum, Person person) {
        this.sum = sum;
        this.person = person;
    }

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "sum")
    private Double sum;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
    private Person person;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "card_id")
    private List<Product> productList = new ArrayList<>();

    public List<Product> getProductList() {
        this.sum = calculateSum();
        return this.productList;
    }

    public Double getSum() {
        this.sum = calculateSum();
        return this.sum;
    }

    private Double calculateSum() {
        Double sumProduct = 0.0;
        for (Product product : productList) {
            sumProduct += product.getPrice();
        }
        return sumProduct;
    }
}
