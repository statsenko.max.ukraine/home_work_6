package com.example.home_work_6.service;

import com.example.home_work_6.modal.Card;
import com.example.home_work_6.modal.Product;

import java.util.List;

public interface CardService {

    Card addCardProduct(Long personId, Long productId);

    List<Card> getAllCard();

    List<Product> getAllCardProduct(Long personId);

    void deleteCardProduct(Long productId);

    Double getCardProductPrice(Long personId);
}
