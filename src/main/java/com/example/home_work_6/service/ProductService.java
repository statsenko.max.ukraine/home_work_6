package com.example.home_work_6.service;

import com.example.home_work_6.modal.Product;

import java.util.List;

public interface ProductService {

    Product createProduct(Product product);

    Product updateProduct(Product product);

    Product getProduct(Long id);

    List<Product> getAllProduct();

    void deleteProduct(Long id);
}
