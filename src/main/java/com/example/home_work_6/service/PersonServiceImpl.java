package com.example.home_work_6.service;

import com.example.home_work_6.modal.Card;
import com.example.home_work_6.modal.Person;
import com.example.home_work_6.repository.CardRepository;
import com.example.home_work_6.repository.PersonRepository;
import com.example.home_work_6.repository.ShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.example.home_work_6.converter.PersonConverter.personConverter;

@Service
public class PersonServiceImpl implements PersonService {

    private final ShopRepository shopRepository;
    private final PersonRepository personRepository;
    private final CardRepository cardRepository;

    @Autowired
    public PersonServiceImpl(ShopRepository shopRepository, PersonRepository personRepository, CardRepository cardRepository) {
        this.shopRepository = shopRepository;
        this.personRepository = personRepository;
        this.cardRepository = cardRepository;
    }

    @Override
    public Person createPerson(Person person) {
        return personRepository.save(person);
    }

    @Override
    public void createPersonCard(Long personId) {
        Person person = personRepository.findById(personId)
                .orElseThrow(() -> new RuntimeException("Person with id: " + personId + " not found"));
        Card card = new Card(0.0, person);
        cardRepository.save(card);
    }

    @Override
    public Person updatePerson(Person person) {
        Person oldPerson = personRepository
                .findById(person.getId())
                .orElseThrow(() -> new RuntimeException("Person with id: " + person.getId() + " not found"));

        return personRepository.save(personConverter(person, oldPerson));
    }

    @Override
    public Person getPerson(Long personId) {
        return personRepository
                .findById(personId)
                .orElseThrow(() -> new RuntimeException("Person with id: " + personId + " not found"));
    }

    @Override
    public List<Person> getAllPerson() {
        List<Person> allPerson = new ArrayList<>();
        personRepository
                .findAll()
                .iterator()
                .forEachRemaining(allPerson::add);

        return allPerson;
    }

    @Override
    public void deletePerson(Long personId) {
        personRepository.deleteById(personId);
    }
}
