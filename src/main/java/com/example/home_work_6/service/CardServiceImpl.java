package com.example.home_work_6.service;

import com.example.home_work_6.modal.Card;
import com.example.home_work_6.modal.Product;
import com.example.home_work_6.repository.CardRepository;
import com.example.home_work_6.repository.PersonRepository;
import com.example.home_work_6.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CardServiceImpl implements CardService {

    private final PersonRepository personRepository;
    private final CardRepository cardRepository;
    private final ProductRepository productRepository;

    @Autowired
    public CardServiceImpl(PersonRepository personRepository, CardRepository cardRepository, ProductRepository productRepository) {
        this.personRepository = personRepository;
        this.cardRepository = cardRepository;
        this.productRepository = productRepository;
    }

    @Override
    public Card addCardProduct(Long personId, Long productId) {
        Card card = cardRepository
                .findById(
                        personRepository
                                .findById(personId)
                                .orElseThrow(() -> new RuntimeException("Person with id: " + personId + " not found"))
                                .getCard()
                                .getId()
                ).orElseThrow(() -> new RuntimeException("Card not found"));

//        cardRepository
//                .findById(card.getId())
//                .orElseThrow(() -> new RuntimeException("Card with id: " + card.getId() + " not found"))
//                .getProductList()
//                .add(productRepository
//                        .findById(productId)
//                        .orElseThrow(() -> new RuntimeException("Product not found")));
        Product product = productRepository.findById(productId).orElseThrow(() -> new RuntimeException("Product not found"));
        product.setCard(card);
        productRepository.save(product);

        return cardRepository.findById(card.getId()).orElseThrow(() -> new RuntimeException("Card not found"));
    }

    @Override
    public List<Card> getAllCard() {
        List<Card> allCard = new ArrayList<>();
        cardRepository
                .findAll()
                .iterator()
                .forEachRemaining(allCard::add);

        return allCard;
    }

    @Override
    public List<Product> getAllCardProduct(Long personId) {
        Card card = personRepository.findById(personId)
                .orElseThrow(() -> new RuntimeException("Person with id: " + personId + " not found"))
                .getCard();

        List<Product> productList = new ArrayList<>();
        productRepository.findAll().iterator().forEachRemaining(e -> {
            if (e.getCard() != null && e.getCard().getId().equals(card.getId())) {
                productList.add(e);
            }
        });
        return productList;
    }

    @Override
    public void deleteCardProduct(Long productId) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new RuntimeException("Product with id: " + productId + " not found"));
        product.setCard(null);

        productRepository.save(product);
    }

    @Override
    public Double getCardProductPrice(Long personId) {
        return personRepository
                .findById(personId)
                .orElseThrow(() -> new RuntimeException("Person with id: " + personId + " not found"))
                .getCard()
                .getSum();
    }
}
