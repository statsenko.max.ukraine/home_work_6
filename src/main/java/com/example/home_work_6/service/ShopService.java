package com.example.home_work_6.service;

import com.example.home_work_6.modal.Shop;

import java.util.List;

public interface ShopService {

    Shop createShop(Shop shop);

    Shop updateShop(Shop shop);

    Shop getShop(Long shopId);

    List<Shop> getAllShop();

    void deleteShop(Long shopId);
}
