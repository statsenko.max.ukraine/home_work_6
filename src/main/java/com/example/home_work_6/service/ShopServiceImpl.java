package com.example.home_work_6.service;

import com.example.home_work_6.modal.Shop;
import com.example.home_work_6.repository.ShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.example.home_work_6.converter.ShopConverter.shopConverter;

@Service
public class ShopServiceImpl implements ShopService {

    private final ShopRepository shopRepository;

    @Autowired
    public ShopServiceImpl(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }

    @Override
    public Shop createShop(Shop shop) {
        return shopRepository.save(shop);
    }

    @Override
    public Shop updateShop(Shop shop) {
        Shop oldShop = shopRepository
                .findById(shop.getId())
                .orElseThrow(() -> new RuntimeException("Card with id: " + shop.getId() + " not found"));

        return shopRepository.save(shopConverter(shop, oldShop));
    }

    @Override
    public Shop getShop(Long shopId) {
        return shopRepository
                .findById(shopId)
                .orElseThrow(() -> new RuntimeException("Card with id: " + shopId + " not found"));
    }

    @Override
    public List<Shop> getAllShop() {
        List<Shop> allShop = new ArrayList<>();
        shopRepository
                .findAll()
                .iterator()
                .forEachRemaining(allShop::add);

        return allShop;
    }

    @Override
    public void deleteShop(Long shopId) {
        shopRepository.deleteById(shopId);
    }
}
