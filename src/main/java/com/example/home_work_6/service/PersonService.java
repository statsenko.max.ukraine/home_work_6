package com.example.home_work_6.service;

import com.example.home_work_6.modal.Person;

import java.util.List;

public interface PersonService {

    Person createPerson(Person person);

    void createPersonCard(Long personId);

    Person updatePerson(Person person);

    Person getPerson(Long id);

    List<Person> getAllPerson();

    void deletePerson(Long personId);
}
