package com.example.home_work_6.service;

import com.example.home_work_6.modal.Product;
import com.example.home_work_6.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.example.home_work_6.converter.ProductConverter.productConverter;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product createProduct(Product product) {
        productRepository.save(product);
        return product;
    }

    @Override
    public Product updateProduct(Product product) {
        Product oldProduct = productRepository
                .findById(product.getId())
                .orElseGet(Product::new);

        return productRepository.save(productConverter(product, oldProduct));
    }

    @Override
    public Product getProduct(Long id) {
        return productRepository
                .findById(id)
                .orElseGet(Product::new);
    }

    @Override
    public List<Product> getAllProduct() {
        List<Product> allProduct = new ArrayList<>();
        productRepository
                .findAll()
                .iterator()
                .forEachRemaining(allProduct::add);

        return allProduct;
    }

    @Override
    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }
}
