package com.example.home_work_6.controller;

import com.example.home_work_6.modal.Product;
import com.example.home_work_6.service.ProductService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/createProduct")
    public Product createProduct(@RequestBody @NonNull Product product) {
        return productService.createProduct(product);
    }

    @GetMapping("/getAllProduct")
    public List<Product> getAllProduct() {
        return productService.getAllProduct();
    }

    @GetMapping("/getProduct{id}")
    public Product getProduct(@PathVariable("id") @NonNull Long id) {
        return productService.getProduct(id);
    }

    @PostMapping("/updateProduct")
    public Product updateProduct(@RequestBody @NonNull Product person) {
        return productService.updateProduct(person);
    }

    @DeleteMapping("/deleteProduct{id}")
    public void deleteProduct(@PathVariable("id") @NonNull Long id) {
        productService.deleteProduct(id);
    }
}
