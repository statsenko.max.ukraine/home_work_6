package com.example.home_work_6.controller;

import com.example.home_work_6.modal.Card;
import com.example.home_work_6.modal.Product;
import com.example.home_work_6.service.CardService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CardController {

    private final CardService cardService;

    @Autowired
    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @GetMapping("/person{personId}/card/addCardProduct{productId}")
    public Card addCardProduct(@PathVariable("personId") @NonNull Long personId, @PathVariable("productId") @NonNull Long productId) {
        return cardService.addCardProduct(personId, productId);
    }

    @GetMapping("person/card/getAllCard")
    public List<Card> getAllCard() {
        return cardService.getAllCard();
    }

    @GetMapping("/person{personId}/card/getAllCardProduct")
    public List<Product> getAllCardProduct(@PathVariable("personId") @NonNull Long personId) {
        return cardService.getAllCardProduct(personId);
    }

    @DeleteMapping("/person/card/deleteCardProduct{productId}")
    public void deleteCardProduct(@PathVariable("productId") @NonNull Long productId) {
        cardService.deleteCardProduct(productId);
    }

    @GetMapping("/person{productId}/card/getCardProductPrice")
    public Double getCardProductPrice(@PathVariable("productId") @NonNull Long productId) {
        return cardService.getCardProductPrice(productId);
    }
}
