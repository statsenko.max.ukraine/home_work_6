package com.example.home_work_6.controller;

import com.example.home_work_6.modal.Shop;
import com.example.home_work_6.service.ShopService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shop")
public class ShopController {

    private final ShopService shopService;

    @Autowired
    public ShopController(ShopService shopService) {
        this.shopService = shopService;
    }

    @PostMapping("/createShop")
    public Shop createShop(@RequestBody @NonNull Shop shop) {
        return shopService.createShop(shop);
    }

    @GetMapping("/getAllShop")
    public List<Shop> getAllShop() {
        return shopService.getAllShop();
    }

    @GetMapping("/getShop{shopId}")
    public Shop getShop(@PathVariable("shopId") @NonNull Long shopId) {
        return shopService.getShop(shopId);
    }

    @PostMapping("/updateShop")
    public Shop updateShop(@RequestBody @NonNull Shop shop) {
        return shopService.updateShop(shop);
    }

    @DeleteMapping("/deleteShop{shopId}")
    public void deleteShop(@PathVariable("shopId") @NonNull Long shopId) {
        shopService.deleteShop(shopId);
    }
}
