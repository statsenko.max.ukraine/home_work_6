package com.example.home_work_6.controller;

import com.example.home_work_6.modal.Person;
import com.example.home_work_6.service.PersonService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping("/createPerson")
    public Person createPerson(@RequestBody @NonNull Person person) {
        return personService.createPerson(person);
    }

    @GetMapping("/createPerson{personId}Card")
    public void createPersonCard(@PathVariable("personId") @NonNull Long personId) {
        personService.createPersonCard(personId);
    }

    @GetMapping("/getAllPerson")
    public List<Person> getAllPerson() {
        return personService.getAllPerson();
    }

    @GetMapping("/getPerson{personId}")
    public Person getPerson(@PathVariable("personId") @NonNull Long personId) {
        return personService.getPerson(personId);
    }

    @PostMapping("/updatePerson")
    public Person updatePerson(@RequestBody @NonNull Person person) {
        return personService.updatePerson(person);
    }

    @DeleteMapping("/deletePerson{personId}")
    public void deletePerson(@PathVariable("personId") @NonNull Long personId) {
        personService.deletePerson(personId);
    }
}
