package com.example.home_work_6.service;

import com.example.home_work_6.controller.ProductController;
import com.example.home_work_6.modal.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ProductControllerTest {
    private Product product;
    private Product createdProduct;

    @Autowired
    private ProductController productController;

    @BeforeEach
    public void createProduct() {
        product = new Product("Bread", 2.0);
        createdProduct = productController.createProduct(product);
    }

    @Test
    void createProductTest() {
        Assertions.assertEquals(product, createdProduct, "Product objects don't match");

        productController.deleteProduct(createdProduct.getId());
    }

    @Test
    void updateProductTest() {
        Product expectedProduct = new Product(createdProduct.getId(), "Milk", 4.2);
        Product actualProduct = productController.updateProduct(expectedProduct);

        Assertions.assertEquals(expectedProduct, actualProduct, "Product objects don't match");

        productController.deleteProduct(actualProduct.getId());
    }

    @Test
    void getProductTest() {
        Product actualProduct = productController.getProduct(createdProduct.getId());

        Assertions.assertEquals(createdProduct, actualProduct, "Product objects don't match");

        productController.deleteProduct(actualProduct.getId());
    }

    @Test
    void getAllProductTest() {
        List<Product> expectedProducts = productController.getAllProduct();
        Product product1 = productController.createProduct(new Product("Milk1", 1.1));
        Product product2 = productController.createProduct(new Product("Milk2", 2.2));
        List<Product> actualProducts = productController.getAllProduct();

        Assertions.assertEquals(expectedProducts.size() + 2, actualProducts.size(), "List products size doesn't match");

        productController.deleteProduct(product1.getId());
        productController.deleteProduct(product2.getId());
    }

    @Test
    void deletePersonTest() {
        productController.deleteProduct(createdProduct.getId());

        Product actualProduct = productController.getAllProduct().stream().filter(v -> v.getId().equals(createdProduct.getId())).findFirst().orElse(null);

        Assertions.assertNull(actualProduct, "Product not removed");
    }
}