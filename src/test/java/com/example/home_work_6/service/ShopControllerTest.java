package com.example.home_work_6.service;

import com.example.home_work_6.controller.ShopController;
import com.example.home_work_6.modal.Shop;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class ShopControllerTest {
    Shop shop;
    Shop createdShop;

    @Autowired
    private ShopController shopController;

    @BeforeEach
    public void createShop() {
        shop = new Shop("ATB");
        createdShop = shopController.createShop(shop);
    }

    @Test
    void createShopTest() {
        Assertions.assertEquals(shop, createdShop, "Shop objects don't match");

        shopController.deleteShop(shop.getId());
    }

    @Test
    void updateShopTest() {
        Shop actualShop = shopController.updateShop(createdShop);

        Assertions.assertEquals(createdShop, actualShop, "Shop objects don't match");

        shopController.deleteShop(actualShop.getId());
    }

    @Test
    void getShopTest() {
        Shop actualShop = shopController.getShop(createdShop.getId());

        Assertions.assertEquals(createdShop, actualShop, "Shop objects don't match");

        shopController.deleteShop(actualShop.getId());
    }

    @Test
    void getAllShopTest() {
        List<Shop> expectedShops = shopController.getAllShop();
        Shop shop1 = shopController.createShop(new Shop("Welmart1"));
        Shop shop2 = shopController.createShop(new Shop("Welmart2"));
        List<Shop> actualShops = shopController.getAllShop();

        Assertions.assertEquals(expectedShops.size() + 2, actualShops.size(), "List shops size doesn't match");

        shopController.deleteShop(shop1.getId());
        shopController.deleteShop(shop2.getId());
    }

    @Test
    void deleteShopTest() {
        shopController.deleteShop(createdShop.getId());

        Shop actualShop = shopController
                .getAllShop()
                .stream()
                .filter(v -> v.getId().equals(shop.getId()))
                .findFirst()
                .orElse(null);

        Assertions.assertNull(actualShop, "Shop not removed");
    }
}
