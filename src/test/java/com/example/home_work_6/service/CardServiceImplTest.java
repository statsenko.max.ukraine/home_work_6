package com.example.home_work_6.service;

import com.example.home_work_6.controller.CardController;
import com.example.home_work_6.controller.PersonController;
import com.example.home_work_6.controller.ProductController;
import com.example.home_work_6.controller.ShopController;
import com.example.home_work_6.modal.Card;
import com.example.home_work_6.modal.Person;
import com.example.home_work_6.modal.Product;
import com.example.home_work_6.modal.Shop;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
class CardServiceImplTest {
    private static Person person;
    private static Product product;

    @Autowired
    private CardController cardController;
    @Autowired
    private ProductController productController;


    @BeforeEach
    public void create(@Autowired ShopController shopController, @Autowired PersonController personController, @Autowired ProductController productController) {
        Shop shop = shopController.createShop(new Shop("ATB"));
        person = personController.createPerson(new Person("Max", "Statsenko", 21, "101", "test@example.com", shop));
        product = productController.createProduct(new Product("Bread", 11.0));
        personController.createPersonCard(person.getId());
    }

    @Test
    void addCardProduct() {
        Card card = cardController.addCardProduct(person.getId(), product.getId());

        List<Product> actualList = productController.getAllProduct()
                .stream()
                .filter(v -> v.getCard() != null && v.getCard().getId().equals(card.getId()))
                .collect(Collectors.toList());

        Assertions.assertEquals(1, actualList.size());
    }

    @Test
    void getAllCardProduct() {
        cardController.addCardProduct(person.getId(), product.getId());
        List<Product> expectedList = cardController.getAllCardProduct(person.getId())
                .stream()
                .filter(v -> v.getCard() != null)
                .collect(Collectors.toList());

        product = productController.createProduct(new Product("Bread", 1.3));
        cardController.addCardProduct(person.getId(), product.getId());
        List<Product> actualList = cardController.getAllCardProduct(person.getId())
                .stream()
                .filter(v -> v.getCard() != null)
                .collect(Collectors.toList());

        Assertions.assertEquals(expectedList.size() + 1, actualList.size());
    }

    @Test
    void deleteCardProduct() {
        cardController.addCardProduct(person.getId(), product.getId());

        List<Product> expectedList = cardController.getAllCardProduct(person.getId())
                .stream()
                .filter(v -> v.getCard() != null)
                .collect(Collectors.toList());

        cardController.deleteCardProduct(product.getId());
        List<Product> actualList = cardController.getAllCardProduct(person.getId())
                .stream()
                .filter(v -> v.getCard() != null)
                .collect(Collectors.toList());

        Assertions.assertEquals(expectedList.size() - 1, actualList.size(), "Products objects don't match");
    }

    @Test
    void getCardProductPrice() {
        cardController.addCardProduct(person.getId(), product.getId());
        Product product1 = productController.createProduct(new Product("Bread", 1.7));
        cardController.addCardProduct(person.getId(), product1.getId());
        Double price = cardController.getCardProductPrice(person.getId());

        Assertions.assertEquals(12.7, price);
    }
}