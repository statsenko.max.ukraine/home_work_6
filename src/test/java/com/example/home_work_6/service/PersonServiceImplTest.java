package com.example.home_work_6.service;

import com.example.home_work_6.controller.CardController;
import com.example.home_work_6.controller.PersonController;
import com.example.home_work_6.controller.ShopController;
import com.example.home_work_6.modal.Card;
import com.example.home_work_6.modal.Person;
import com.example.home_work_6.modal.Shop;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class PersonServiceImplTest {
    private static Shop shop;
    private Person person;
    private Person createdPerson;

    @Autowired
    private PersonController personController;
    @Autowired
    private CardController cardController;

    @BeforeAll
    public static void createShop(@Autowired ShopController shopController) {
        shop = shopController.createShop(new Shop("ATB"));
    }

    @BeforeEach
    public void createPerson() {
        person = new Person("Max", "Statsenko", 11, "103", "test@example.com", shop);
        createdPerson = personController.createPerson(person);
    }

    @Test
    void createPersonTest() {
        Assertions.assertEquals(person, createdPerson, "Person objects don't match");

        personController.deletePerson(createdPerson.getId());
    }

    @Test
    void createPersonCardTest() {
        personController.createPersonCard(createdPerson.getId());
        Card card = cardController
                .getAllCard()
                .stream()
                .filter(v -> v.getPerson().getId().equals(createdPerson.getId()))
                .findFirst()
                .orElse(null);

        Assertions.assertNotNull(card);
    }

    @Test
    void updatePersonTest() {
        Person expectedPerson = new Person(createdPerson.getId(), "Test", "Test", 19, "101", "test@example.com");
        Person actualPerson = personController.updatePerson(expectedPerson);

        Assertions.assertEquals(expectedPerson, actualPerson, "Person objects don't match");

        personController.deletePerson(actualPerson.getId());
    }

    @Test
    void getPersonTest() {
        Person actualPerson = personController.getPerson(createdPerson.getId());

        Assertions.assertEquals(createdPerson, actualPerson, "Person objects don't match");

        personController.deletePerson(actualPerson.getId());
    }

    @Test
    void getAllPersonTest() {
        List<Person> expectedPersons = personController.getAllPerson();
        Person person1 = personController.createPerson(new Person("Max1", "Statsenko", 11, "103", "test@example.com", shop));
        Person person2 = personController.createPerson(new Person("Max2", "Statsenko", 11, "103", "test@example.com", shop));
        List<Person> actualPersons = personController.getAllPerson();

        Assertions.assertEquals(expectedPersons.size() + 2, actualPersons.size(), "List persons size doesn't match");

        personController.deletePerson(person1.getId());
        personController.deletePerson(person2.getId());
    }

    @Test
    void deletePersonTest() {
        personController.deletePerson(createdPerson.getId());

        Person actualPerson = personController.getAllPerson().stream().filter(v -> v.getId().equals(createdPerson.getId())).findFirst().orElse(null);

        Assertions.assertNull(actualPerson, "Person not removed");
    }

    @AfterAll
    public static void deleteShop(@Autowired ShopController shopController) {
        shopController.deleteShop(shop.getId());
    }
}